﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class QuakeBob : MonoBehaviour {
    //Simulate classic Quake gun bobbing!
    //Jack Patterson
    
    private bool moving = false;
    private Vector3 curpos;
    private float curposz = 0.0f;
    private float posoffseti = 0.0f;
    private int curdir = 1; // 1 = Forward 2 = Backward
    private Vector3 lastpos = new Vector3(0,0,0);   
    private Vector3 origin;

    private void Start() {
        lastpos = transform.parent.position;
        origin = transform.localPosition;
    }

    void Update () {
        //Vector3 product = lastpos - transform.parent.position;
        //if (product != new Vector3(0,0,0)){moving = true;}else {moving = false;}
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)) {
            if (posoffseti <= 0.2f && curdir == 1){
                posoffseti += 0.025f; //0.025
            }else {curdir = 2;}
            if(posoffseti >= -0.2f && curdir == 2) {
                posoffseti -= 0.025f;
            }else{curdir = 1;}
            curpos = transform.localPosition;
            curpos.z = origin.z - posoffseti;
            transform.localPosition = curpos;
        }else {transform.localPosition = Vector3.Lerp(transform.localPosition,origin,0.1f); posoffseti = 0;}

        //lastpos = transform.parent.position;
    }
}
