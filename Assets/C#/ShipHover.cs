﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHover : MonoBehaviour {

    float offseti = 0;
    float offsetdir = 1; //1 - Down 2 - Upward
    Vector3 pos = new Vector3();
    Vector3 origin = new Vector3();

    private void Start() {
        origin = transform.position;
    }

    void Update () {
		pos = transform.position;
        if (offseti >= -0.3f && offsetdir == 1){
            offseti -= 0.003f;
        }else{offsetdir = 2;}
        if(offseti <= 0f && offsetdir == 2) {
            offseti += 0.003f;
        } else { offsetdir = 1; }
        pos.y = origin.y - offseti;
        transform.position = pos;
    }
}
