﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockpitPlayer : MonoBehaviour {

    private float xRotate;
    private float yRotate;
    private float xTarget;
    private float yTarget;

    private void Start() {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update () {
        xTarget += Input.GetAxis("Mouse X");
        //yTarget += Input.GetAxis("Mouse Y");

        xRotate = Mathf.Lerp(xRotate, xTarget,0.3f);
        //yRotate = Mathf.Lerp(yRotate,yTarget,0.3f);

        transform.rotation  = Quaternion.Euler(0,xRotate,0);
	}
}
